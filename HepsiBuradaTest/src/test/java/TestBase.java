
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
public class TestBase {
    static WebDriver driver;

    public void Start() {
        System.setProperty("webdriver.chrome.driver", "properties/driver/chromedriver.exe");
        this.driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.hepsiburada.com/");
    }
    public void Finish() {
        driver.quit();
    }
    public static WebElement findElement(By by) {
        return  driver.findElement(by);
    }
    public  static  void Click(By by) {
        findElement(by).click();
    }
    public static void Send(By by,String isim_soyisim){
        findElement(by).sendKeys(isim_soyisim);
    }
    public static String isimKontrol(By by){

        return findElement(by).getText();
    }
    public static void Title(String url){
        Assert.assertEquals(url,driver.getTitle());
    }
}