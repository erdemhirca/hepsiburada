import com.thoughtworks.gauge.Step;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.junit.Test;


public class HepsiBuradaTestleri1 extends TestBase {
    WebDriver driver;

    @Test
    @Step("HepsiBurada Anasayfasına git")
    public void AnasayfaAc() {
        Start();
        Title("Türkiye'nin En Büyük Online Alışveriş Sitesi Hepsiburada.com");
        System.out.println("Senaryo:1 Hespsiburada sayfasi tarayicida acildi");
    }
    @Test
    @Step("Kullanıcı Girisi Butonuna Tıkla")
    public void GirişButonu() throws InterruptedException {
        Click(By.xpath("//*[@id=\"myAccount\"]/span/span[1]"));
        Thread.sleep(2000);
        Click(By.xpath("//*[@id=\"login\"]"));
        System.out.println("Senaryo:2 Butona tiklandi.");
    }
    @Test
    @Step("Kullanıcı Bilgilerini Gir <kullanici_adi> <sifre>")
    public void Logın(String kullanici_adi, String sifre) throws InterruptedException {
        Thread.sleep(2000);
        Title("Üye Giriş Sayfası & Üye Ol - Hepsiburada");
        Send(By.id("txtUserName"), (kullanici_adi));
        Send(By.id("txtPassword"), (sifre));
        Click(By.id("btnLogin"));
        System.out.println("Senaryo:3 Giris yapildi.");
        Thread.sleep(2000);
    }
    @Test
    @Step("Kullanıcı İsmi Kontrol <isim_soyisim>")
    public void İsimKontrol(String isim_soyisim) throws InterruptedException {
        Thread.sleep(2000);
        Title("Türkiye'nin En Büyük Online Alışveriş Sitesi Hepsiburada.com");
        String kullaniciisim = isimKontrol(By.xpath("//*[@id=\"myAccount\"]/span/a/span[2]"));
        if (isim_soyisim.equals(kullaniciisim)) {
            System.out.println("Senaryo:4 isim Soyisim kontrolu yapildi ve Eslesti.Kullanici Ismi=" +kullaniciisim);
        } else {
            System.out.println("Senaryo:4 isim Soyisim kontrolu yapildi ve Eslesmedi.");
        }
    }
    @Test
    @Step("Hepsiburada Site Kapanıs")
    public void kapanıs() throws InterruptedException {
        Thread.sleep(2000);
        Finish();
        System.out.println("Senaryo:5 Test Basarili ve Tarayici Kapandi");
    }
}
